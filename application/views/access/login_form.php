<div style="height: 100vh">
  <div class="flex-center flex-column">

  <div style="border: 1px solid gray" class="p-3">
    <h1 class="mt-3 text-center">Entrar</h1>
    <form method="POST">
      <div class="form-outline mb-4">
        <input type="email" id="email" name="email" class="form-control" />
        <label class="form-label" for="form2Example1">Email</label>
      </div>
      <div class="form-outline mb-4">
        <input type="password" id="senha" name="senha" class="form-control" />
        <label class="form-label" for="form2Example2">Senha</label>
      </div>
      <button type="submit" class="btn btn-primary btn-block mb-4">
        Entrar
      </button>        
    </form>

    <p class="red-text"><?= $error ? 'Dados de acesso incorretos.' : ''?></p>
    </div>

    </div>
  </div>
</div>