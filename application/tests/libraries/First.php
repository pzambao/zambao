<?php

class First extends TestCase {
    public function setUp(): void {
        $this->resetInstance();
    }

    function xtestHello() {
        $this->CI->load->library('conta');
        $res = $this->CI->conta->soma(3, 2);
        $this->assertEquals(5, $res);
    }

    function xtestContaDeveRetornarRegistrosDoMesDezembro() {
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 12, 2020);
        $this->assertEquals(4, sizeof($res));
    }

    function xtestContaDeveInserirRegistroCorretamente() {

        $this->assertEquals(1, sizeof($res));

    }

    function testContaDeveInserirRegistroCorretamente() {
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 1, 2021);

        $this->assertEquals('Magalu', $res[0]['parceiro']);
        $this->assertEquals(2021, $res[0]['ano']);
        $this->assertEquals(1, $res[0]['mes']);
        $this->assertEquals(3, sizeof($res));

        $this->assertEquals('Casas Bahia', $res[1]['parceiro']);
        $this->assertEquals(2021, $res[1]['ano']);
        $this->assertEquals(1, $res[1]['mes']);

        $this->assertEquals('Bandeirante', $res[2]['parceiro']);
        $this->assertEquals(2021, $res[2]['ano']);
        $this->assertEquals(2021, $res[2]['ano']);
        $this->assertEquals(1, $res[2]['mes']);
    }

    function testContaDeveInformarTotalDeContasAPagarEAReceber() {
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->total('pagar', 1, 2021);
        $this->assertEquals(5097.25, $res);
    }

    function testContaCalcularSaldoMensal(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->saldo(1, 2021);

        $this->assertEquals(-875.07, $res);
    }
}